package it.nesea.corsojava.modulo2;

import java.util.HashMap;
import java.util.Map;

public class Sorteggio {
    private static final Map<Integer, String> ALUNNI;

    static {
        ALUNNI = new HashMap<>();
        ALUNNI.put(1, "Luca");
        ALUNNI.put(2, "Ferdinando");
        ALUNNI.put(3, "Fabio");
        ALUNNI.put(4, "Bruno");
        ALUNNI.put(5, "Simone");
        ALUNNI.put(6, "Giacomo");
        ALUNNI.put(7, "Joele");
        ALUNNI.put(8, "Ivano");
        ALUNNI.put(9, "Cesare");
        ALUNNI.put(10, "Fabrizio");
        ALUNNI.put(11, "Riccardo");
        ALUNNI.put(12, "Roberto");
        ALUNNI.put(13, "Rosy");
        ALUNNI.put(14, "Paola");
        ALUNNI.put(15, "Antonio");
        ALUNNI.put(16, "Cristian");
        ALUNNI.put(17, "Simona");
        ALUNNI.put(18, "Marco");
        ALUNNI.put(19, "Giuliano");
    }

    public static void main(String[] args) {
        int luckyNumber = (int) (1 + 19 * Math.random());
        System.out.print("And the winner is... " + ALUNNI.get(luckyNumber));
    }
}
