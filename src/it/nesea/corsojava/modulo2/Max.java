package it.nesea.corsojava.modulo2;

import it.nesea.epj.modulo1.Dipendente;
import it.nesea.epj.modulo1.Dirigente;

import java.util.ArrayList;
import java.util.List;

public class Max {
    public static void main(String[] args) {
        List<Double> test1 = new ArrayList<>();
        test1.add(Math.PI);
        test1.add(3.21);
        test1.add(-3.0);
        test1.add(5 * Math.random());
        System.out.println("Il max di " + test1 + " è " + maxValue(test1));

        List<Character> test2 = new ArrayList<>();
        test2.add('(');
        test2.add('ü');
        test2.add('/');
        test2.add('E');
        System.out.println("Il max di " + test2 + " è " + maxValue(test2));

        List<Integer> test3 = new ArrayList<>();
        System.out.println("Il max di " + test3 + " è " + maxValue(test3)); //testando su List vuota

        List<Integer> test4 = new ArrayList<>();
        test4.add(null);
        test4.add(null);
        test4.add(null);
        System.out.println("Il max di " + test4 + " è " + maxValue(test4)); //testando su List di soli null

        List<Dipendente> test5 = new ArrayList<>();
        test5.add(new Dirigente("Luca Bianchi", 1500.0, 200.0));
        test5.add(new Dipendente("Mario Rossi", 1200.0));
        test5.add(new Dipendente("Marco Verdi", 1200.0));
        test5.add(new Dirigente("Maria Gialli", 1400.0, 400.0));
        System.out.println("Il max di " + test5 + " è " + maxValue(test5)); //testando su List con sottoclassi
    }

    private static <T extends Comparable<? super T>> T maxValue(List<T> input) {
        if (input.size() == 0) return null;
        T max = null;

        for (T t : input) {
            if (t != null && max == null)
                max = t; // il primo valore non null di input diventa il valore iniziale di max
            if (t != null && t.compareTo(max) > 0) max = t;
        }
        return max;
    }
}
