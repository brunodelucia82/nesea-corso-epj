package it.nesea.corsojava.modulo2;

import java.util.ArrayList;
import java.util.List;

public class Hanoi {
    public static void main(String[] args) {
        int maxSize = 7;
        Rod rodA = new Rod('A');
        Rod rodB = new Rod('B');
        Rod rodC = new Rod('C');
        for (int i = maxSize; i > 0; i--) {
            Disk disk = new Disk(i, rodA);
            rodA.addDisk(disk);
        }
        System.out.println("Situazione iniziale");
        printRods(maxSize + 1, rodA, rodB, rodC);
        System.out.println();
        String[] solution = solveHanoi(maxSize, rodA, rodB, rodC);
        int movesCountDigits = Integer.toString(solution.length).length();
        System.out.println("Elenco mosse");
        for (int i = 0; i < solution.length; i++) {
            System.out.printf("%" + movesCountDigits + "d. %s\n", i + 1, solution[i]);
        }
        System.out.println();
        System.out.println("Situazione finale");
        printRods(maxSize + 1, rodA, rodB, rodC);
    }

    private static String[] solveHanoi(int disksAmount, Rod rodA, Rod rodB, Rod rodC) {
        ArrayList<String> moves = new ArrayList<>();
        if (rodA.getDisks().size() % 2 == 0) {
            while (rodC.getDisks().size() != disksAmount) {
                try {
                    moves.add(rodA.moveTo(rodB));
                } catch (Exception e) {
                    moves.add(rodB.moveTo(rodA));
                }
                try {
                    if (rodC.getDisks().size() != disksAmount) moves.add(rodA.moveTo(rodC));
                } catch (Exception e) {
                    moves.add(rodC.moveTo(rodA));
                }
                try {
                    if (rodC.getDisks().size() != disksAmount) moves.add(rodB.moveTo(rodC));
                } catch (Exception e) {
                    moves.add(rodC.moveTo(rodB));
                }
            }
        } else {
            while (rodC.getDisks().size() != disksAmount) {
                try {
                    moves.add(rodA.moveTo(rodC));
                } catch (Exception e) {
                    moves.add(rodC.moveTo(rodA));
                }
                try {
                    if (rodC.getDisks().size() != disksAmount) moves.add(rodA.moveTo(rodB));
                } catch (Exception e) {
                    moves.add(rodB.moveTo(rodA));
                }
                try {
                    if (rodC.getDisks().size() != disksAmount) moves.add(rodB.moveTo(rodC));
                } catch (Exception e) {
                    moves.add(rodC.moveTo(rodB));
                }
            }
        }
        return moves.toArray(new String[]{});
    }

    private static void printRods(int maxSize, Rod a, Rod b, Rod c) {
        for (int i = 0; i < maxSize + 1; i++) {
            System.out.print(a.toString(maxSize).toCharArray()[i] + " ");
            System.out.print(b.toString(maxSize).toCharArray()[i] + " ");
            System.out.print(c.toString(maxSize).toCharArray()[i] + " ");
            System.out.println();
        }
    }
}

class Disk {
    private int number;
    private int position;
    private Rod currentRod;

    Disk(int number, Rod currentRod) {
        this.number = number;
        this.position = 0;
        this.currentRod = currentRod;

    }

    int getNumber() {
        return number;
    }

    void setPosition(int position) {
        this.position = position;
    }

    String moveTo(Rod destinationRod) {
        if (this.position != 0 || !destinationRod.canReceiveDisk(this))
            throw new IllegalArgumentException("Illegalmove");
        String r = "Sposto il disco num " + number + " da " + currentRod.getIdentifier() +
                " a " + destinationRod.getIdentifier();
        this.currentRod.removeDisk();
        this.currentRod.updateDisksPositions();
        destinationRod.addDisk(this);
        destinationRod.updateDisksPositions();
        this.currentRod = destinationRod;
        return r;
    }

    @Override
    public String toString() {
        return "Disk{" +
                "number=" + number +
                ", position=" + position +
                '}';
    }
}

class Rod {
    private char identifier;
    private List<Disk> disks;

    Rod(char identifier) {
        this.identifier = identifier;
        disks = new ArrayList<>();
    }

    char getIdentifier() {
        return identifier;
    }

    List<Disk> getDisks() {
        return disks;
    }

    String moveTo(Rod destinationRod){
        return disks.get(0).moveTo(destinationRod);
    }

    void addDisk(Disk newDisk) {
        this.disks.add(0, newDisk);
        this.updateDisksPositions();
    }

    void removeDisk() {
        this.disks.remove(0);
    }

    void updateDisksPositions() {
        for (Disk disk : disks) {
            disk.setPosition(disks.indexOf(disk));
        }
    }

    boolean canReceiveDisk(Disk newDisk) {
        if (disks.size() == 0) {
            return true;
        } else return disks.get(0).getNumber() >= newDisk.getNumber();
    }

    String toString(int rodHeight) {
        StringBuilder r = new StringBuilder();
        r.append(this.identifier);
        for (int i = 0; i < rodHeight; i++) {
            if (disks.size() < rodHeight - i) {
                r.append("|");
            } else {
                r.append(disks.get(i - (rodHeight - disks.size())).getNumber());
            }
        }
        return r.toString();
    }

    @Override
    public String toString() {
        return "Rod{" +
                "identifier=" + identifier +
                ", disks=" + disks +
                '}';
    }

}
