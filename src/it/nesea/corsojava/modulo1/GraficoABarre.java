package it.nesea.corsojava.modulo1;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class GraficoABarre {
    public static void main(String[] args) {
        int[] inputs = getSeveralIntValuesFromUser();
        printChart(inputs);
    }

    private static int[] getSeveralIntValuesFromUser() {
        Scanner in = new Scanner(System.in);
        System.out.println("Please give me some integer values. Negative values will be ignored. The first non-space " +
                "non-digit token will break the input");
        int[] r = new int[0];
        while (in.hasNextInt() || r.length == 0) {
            try {
                int val = in.nextInt();
                if (val >= 0) {
                    r = Arrays.copyOf(r, r.length + 1);
                    r[r.length - 1] = val;
                }
            } catch (InputMismatchException ex) {
                System.err.println("Input Mismatch detected");
                in.next();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return r;
    }

    private static int getMaxValueFromArray(int max, int[] inputs) {
        if (inputs.length == 0) {
            return max;
        } else if (max < inputs[inputs.length - 1]) {
            return getMaxValueFromArray(inputs[inputs.length - 1], Arrays.copyOf(inputs, inputs.length - 1));
        } else {
            return getMaxValueFromArray(max, Arrays.copyOf(inputs, inputs.length - 1));
        }
    }

    private static void printAsteriskOrSpace(int index, int height, int[] inputs){
        if (height<=inputs[index]){
            System.out.print("*");
        } else {
            System.out.print(" ");
        }
    }

    private static void printChart(int[] inputs){
        int maxHeight = getMaxValueFromArray(-1, inputs);
        for (int i = maxHeight; i > 0; i--) {
            for (int j = 0; j < inputs.length; j++) {
                printAsteriskOrSpace(j, i, inputs);
            }
            System.out.println();
        }
    }
}
