package it.nesea.corsojava.modulo1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class NumeriCasuali {
    public static void main(String[] args) {
        int n = getSingleIntValueFromUser(2, 20);
        int m = getSingleIntValueFromUser(n, 10000 * n);
        int[] myRandomValues = generateRandomIntValues(n, m);
        printFrequenciesAndDeltas(myRandomValues);
    }

    private static double[] generateDeltas(int[] occurrencesArr, double avg) {
        double[] r = new double[occurrencesArr.length];
        for (int i = 0; i < occurrencesArr.length; i++) {
            r[i] = occurrencesArr[i] / avg - 1;
        }
        return r;
    }

    private static void printFrequenciesAndDeltas(int[] myRandomValues) {
        int len = Integer.toString(myRandomValues.length).length();
        double avg = sumAll(myRandomValues) / ((double) myRandomValues.length);
        double[] myDeltas = generateDeltas(myRandomValues, avg);
        for (int i = 0; i < myRandomValues.length; i++) {
            System.out.printf("Value %" + len + "d: %5d occurrences, delta is %+.3f (%+.3f%%)\n",
                    (i + 1), myRandomValues[i], myDeltas[i], 100* myDeltas[i]);
        }
    }

    private static int sumAll(int[] myRandomValues) {
        int sum = 0;
        for (int val : myRandomValues) {
            sum += val;
        }
        return sum;
    }

    private static int getSingleIntValueFromUser(int minimum, int maximum) {
        int r = minimum - 1;
        Scanner in = new Scanner(System.in);
        System.out.print("Please give me an int value not lower than " + minimum + " and not greater than " +
                maximum + ": ");
        while (r < minimum || r > maximum) {
            try {
                r = in.nextInt();
                if (r < minimum || r > maximum) {
                    System.out.print("Value must be at least " + minimum + " and not greater than " +
                            maximum + "! Try again: ");
                }
            } catch (InputMismatchException ex) {
                System.out.print("Value must be an integer! Try again: ");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                in = new Scanner(System.in);
            }
        }
        return r;
    }

    private static int generateRandom(int min, int max) {
        return (int) (min + max * Math.random());
    }

    private static int[] generateRandomIntValues(int n, int m) {
        int[] r = new int[n];
        for (int i = 0; i < m; i++) {
            int randomValue = generateRandom(1, n);
            r[randomValue - 1]++;
        }
        return r;
    }
}
