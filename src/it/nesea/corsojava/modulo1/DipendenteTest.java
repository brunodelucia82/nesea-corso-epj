package it.nesea.corsojava.modulo1;

public class DipendenteTest {
    public static void main(String[] args) {
        Dipendente dip2 = new Dipendente("Marco", "Montezucchero", "Frascati",
                "14/05/1984", "MNZMRC84M14D773F", 1325.00,
                "1 Hacker Way, 94025 Menlo Park, CA", "m.monteZUCCHERO@nesea.it",
                "066748511", "069475232", "32488514252");
        System.out.println(dip2);
        dip2.getContatti().setPhoneNumber("01749547741", 5);
        dip2.getContatti().setAddress("via Faccialibro 123, 00100 Roma");
        System.out.println(dip2);
    }
}
