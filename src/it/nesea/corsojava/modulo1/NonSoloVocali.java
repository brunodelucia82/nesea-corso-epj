package it.nesea.corsojava.modulo1;

import java.text.Normalizer;

public class NonSoloVocali {
    public static void main(String[] args) {
        String test1 = "Contare Caratteri Alfabetici";
        String test2 = "Cőntàre Carätteri Alfabètici";
        System.out.println(test1);
        printCounting(countLetters(removeDiacritics(test1)));
        System.out.println(test2);
        printCounting(countLetters(removeDiacritics(test2)));
    }

    /**
     * This method returns an array of int. The array length is twice the length of the alphabet.
     * Each element of the array stores the amount of occurrences for a specific letter. The first
     * element stores the occurrences of 'a', then the second element stores the occurrences of 'b'
     * and so on. The last element stores the occurrences of capitol 'Z'
     * */
    private static int[] countLetters(String normalizedInput) {
        int[] r = new int[('z' - 'a') + ('Z' - 'A') + 2];
        for (int i = 0; i < normalizedInput.length(); i++) {
            char currentChar = normalizedInput.charAt(i);
            if (Character.isAlphabetic(currentChar)) {
                int index;
                if (currentChar > 'Z') {
                    index = currentChar - 'a';
                } else {
                    index = 'z' - 'a' + 1 + (currentChar - 'A');
                }
                if (index > -1 && index < r.length) {
                    r[index]++;
                }
            }
        }
        return r;
    }

    /**
     * This method gets a string object as input and returns the string without
     * diacritics. For instance, if the input string contains either
     * 'a', 'à', 'ä', 'á', 'å' they'll all be converted into 'a' in the output
     * string. Please notice that special letters like 'ø', 'ß', 'ł', 'æ'
     * will not be affected
     */
    private static String removeDiacritics(String input) {
        String nrml = Normalizer.normalize(input, Normalizer.Form.NFD); //separating diacritics from letters
        StringBuilder stripped = new StringBuilder();
        for (int i = 0; i < nrml.length(); ++i) {
            if (Character.getType(nrml.charAt(i)) != Character.NON_SPACING_MARK) {  //filtering non-diacritics
                stripped.append(nrml.charAt(i));                                    //into stringbuilder
            }
        }
        return stripped.toString();
    }

    private static void printCounting(int[] input) {
        for (int i = 0; i < input.length; i++) {
            if (input[i] > 0) {
                if (i < ('z' - 'a') + 1) {
                    System.out.println(((char) ('a' + i)) + ": " + input[i] + " times");
                } else {
                    System.out.println(((char) ('A' + i - ('z' - 'a' + 1))) + ": " + input[i] + " times");
                }
            }
        }
    }
}
