package it.nesea.corsojava.modulo1;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Duplicati {
    public static void main(String[] args) {
        int[] myInputs;
        myInputs = getSeveralIntValuesFromUser();
        System.out.println(reportDuplicated(myInputs));
    }

    public static int[] getSeveralIntValuesFromUser() {
        Scanner in = new Scanner(System.in);
        System.out.println("Please input integer values");
        int[] r = new int[0];
        while (in.hasNextInt()) {
            try {
                int val = in.nextInt();
                r = Arrays.copyOf(r, r.length + 1);
                r[r.length - 1] = val;
            } catch (InputMismatchException ex) {
                System.err.println("Input Mismatch detected");
                in.next();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return r;
    }

    public static boolean isFirstOfManyInstances(int[] arr, int index) throws ArrayIndexOutOfBoundsException {
        if (index > arr.length - 1) {
            throw new ArrayIndexOutOfBoundsException();
        }
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == arr[index]) {
                count++;
                if (count == 1 && i < index) {
                    return false;
                }
                if (count > 1) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String reportDuplicated(int[] arr) {
        boolean anyDuplicated = false;
        String r = "valori duplicati:";
        for (int i = 0; i < arr.length; i++) {
            if (isFirstOfManyInstances(arr, i)){
                anyDuplicated = true;
                r += " " + arr[i];
            }
        }
        if (!anyDuplicated){
            return "non ci sono duplicati";
        } else{
            return r;
        }
    }
}