package it.nesea.corsojava.modulo1;

import java.util.Arrays;
import java.util.Scanner;

public class OrdinaCaratteri {
    public static void main(String[] args) {
        System.out.println("Please give me a string");
        String str = getInputStringFromUser(new Scanner(System.in));
        System.out.println("Here's your string in alphabetic order:");
        System.out.println(alphabeticOrder(str));
    }
    private static String getInputStringFromUser(Scanner in){
        String r = "";
        try {
            r = in.nextLine();
        } catch (Exception e) {
            System.err.println("An error occurred while processing the input! Returning empty String");
            e.printStackTrace();
        }
        return r;
    }
    private static String alphabeticOrder(String in){
//        getting a string as parameter and returning a string containing the original string's letters
//        in alphabetic order (uppercase letters alphabetically ordered first, then lowercase letters alphabetically ordered)
        char[] arr = in.toCharArray();
        Arrays.sort(arr);
        StringBuilder r = new StringBuilder();
        for (char c: arr) {
            if (Character.isLetter(c)){
                r.append(c);
            }
        }
        return r.toString();
    }
}
