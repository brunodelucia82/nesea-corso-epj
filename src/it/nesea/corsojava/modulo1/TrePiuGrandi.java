package it.nesea.corsojava.modulo1;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class TrePiuGrandi {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int positiveNumbersFound = 0;
        System.out.println("Please give me non negative integers (at least three) separated by spaces. " +
                "Negative values or any non integer input will abort input reading");
        int first = -1, second = -1, third = -1;
        int myInt = -1;
        while (positiveNumbersFound < 3) {
            while (in.hasNext()) {
                try {
                    myInt = in.nextInt();
                    if (myInt >= 0) {
                        positiveNumbersFound++;
                        if (myInt > first) {
                            third = second;
                            second = first;
                            first = myInt;
                        } else if (myInt > second) {
                            third = second;
                            second = myInt;
                        } else if (myInt > third) {
                            third = myInt;
                        }
                    } else {
                        break;
                    }
                } catch (InputMismatchException ex) {
                    break;
                    //not an integer or outside the int range
                } catch (NoSuchElementException ex) {
                    break;
                    //input is exhausted
                } catch (IllegalStateException ex) {
                    break;
                    //scanner is closed
                } catch (Exception ex) {
                    System.out.println("Something went wrong");
                    ex.printStackTrace();
                }
            }
            if (positiveNumbersFound >= 3) {
                System.out.println("The three max values are: " + first + ", " + second + ", " + third);
            } else {
                System.out.println("The input had less than 3 non negative integers, try again!");
                positiveNumbersFound = 0;
                first = -1;
                second = -1;
                third = -1;
                myInt = -1;
                in = new Scanner(System.in);
            }
        }
    }
}
