package it.nesea.corsojava.modulo1;

import java.util.Scanner;

public class CifreLettere {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int myNumber = -1;
        while (myNumber < 0) {
            System.out.println("Please give me a non negative integer");
            try {
                myNumber = in.nextInt();
                if (myNumber >=0) {
                    printDigits(myNumber);
                } else {
                    System.out.println("The value must be 0 or greater! Try again");
                }
            } catch (Exception e) {
                System.out.println("Value must be a valid (non negative) integer! Try again");
                in = new Scanner(System.in);
            }
        }
    }

    public static int digitsNumber(Integer myNumber) {
        return myNumber.toString().length();
    }

    public static void printDigits(int myValue) {
        System.out.print(myValue + ": ");
        for (int i = digitsNumber(myValue) - 1; i > -1; i--) {
            int myDigit = (int) ((myValue / Math.pow(10, i)) % 10);
            System.out.print(digitToText(myDigit));
            if (i != 0) {
                System.out.print(" ");
            }
        }
        System.out.println();
    }

    public static String digitToText(int myDigit) {
        switch (myDigit) {
            case 0:
                return "zero";
            case 1:
                return "uno";
            case 2:
                return "due";
            case 3:
                return "tre";
            case 4:
                return "quattro";
            case 5:
                return "cinque";
            case 6:
                return "sei";
            case 7:
                return "sette";
            case 8:
                return "otto";
            case 9:
                return "nove";
        }
        return "";
    }
}
