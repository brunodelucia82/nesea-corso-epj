package it.nesea.corsojava.modulo1;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This program generates and prints out a two-dimensional array filled with values ranging from 1 to
 * n, where n equals height * width. The values will be filled in a spiral movement, starting from one
 * of the four corners and spiraling inwards. The user will be allowed to choose the spiral's height,
 * width, the starting corner and the spiral's direction (either clockwise or counterclockwise)
 *
 * @author Bruno De Lucia
 */
public class Spirale {
    private enum Corner {TOP_LEFT, TOP_RIGHT, BOTTOM_RIGHT, BOTTOM_LEFT}

    private enum Direction {RIGHT, DOWN, LEFT, UP}

    private enum Spin {CLOCKWISE, COUNTERCLOCKWISE}

    public static void main(String[] args) {
        int height = getSingleIntValueFromUser(3, 11, "height");
        int width = getSingleIntValueFromUser(3, 11, "width");
        Corner startPoint = startingPointMenu();
        Spin spin = spinMenu();
        int[][] mySpiral = generateSpiral(height, width, startPoint, spin);
        printSpiral(mySpiral);
    }

    private static int getSingleIntValueFromUser(int minimum, int maximum, String what) {
        int r = minimum - 1;
        Scanner in = new Scanner(System.in);
        System.out.print("Please give me an int value not lower than " + minimum + " and not greater than " +
                maximum + ", this will be the spiral's " + what + ": ");
        while (r < minimum || r > maximum) {
            try {
                r = in.nextInt();
                if (r < minimum || r > maximum) {
                    System.out.print("Value must be at least " + minimum + " and not greater than " +
                            maximum + "! Try again: ");
                }
            } catch (InputMismatchException ex) {
                System.out.print("Value must be an integer! Try again: ");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                in = new Scanner(System.in);
            }
        }
        return r;
    }

    /**
     * This method allows the user to choose the starting corner for the spiral from a menu.
     * The choice will be made by inputting an integer corresponding to the desired corner
     *
     * @return an element of the Corner enum {TOP_LEFT, TOP_RIGHT, BOTTOM_RIGHT, BOTTOM_LEFT}
     */
    private static Corner startingPointMenu() {
        int r = 0;
        Scanner in = new Scanner(System.in);
        System.out.println("Choose the starting point for the spiral:");
        Corner[] cornerArr = Corner.values();
        for (int i = 0; i < cornerArr.length; i++) {
            int digits = Integer.toString(cornerArr.length).length();
            System.out.printf("%" + digits + "d-%s\n", (i + 1), cornerArr[i]);
        }
        while (r < 1 || r > cornerArr.length) {
            try {
                r = in.nextInt();
                if (r < 1 || r > cornerArr.length) {
                    System.out.print("Only values 1 to " + cornerArr.length + " allowed! Try again: ");
                }
            } catch (InputMismatchException ex) {
                System.out.print("Value must be an integer! Try again: ");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                in = new Scanner(System.in);
            }
        }
        return cornerArr[r - 1];
    }

    /**
     * This method allows the user to choose the spinning direction for the spiral from a menu.
     * The choice will be made by inputting an integer corresponding to the desired direction
     *
     * @return an element of the Spin enum {CLOCKWISE, COUNTERCLOCKWISE}
     */
    private static Spin spinMenu() {
        int r = 0;
        Scanner in = new Scanner(System.in);
        System.out.println("Choose the spinning direction for the spiral:");
        Spin[] spinArr = Spin.values();
        for (int i = 0; i < spinArr.length; i++) {
            int digits = Integer.toString(spinArr.length).length();
            System.out.printf("%" + digits + "d-%s\n", (i + 1), spinArr[i]);
        }
        while (r < 1 || r > spinArr.length) {
            try {
                r = in.nextInt();
                if (r < 1 || r > spinArr.length) {
                    System.out.print("Only values 1 to " + spinArr.length + " allowed! Try again: ");
                }
            } catch (InputMismatchException ex) {
                System.out.print("Value must be an integer! Try again: ");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                in = new Scanner(System.in);
            }
        }
        return spinArr[r - 1];
    }

    /**
     * This method generates and returns a two-dimensional array of integers. The values will range from 1 to
     * width * height and follow a spiral movement
     *
     * @param height        the array's height (i.e. amount of lines)
     * @param width         the array's width (i.e. amount of columns)
     * @param startingPoint the array's corner having value of 1
     * @param spin          whether the spiral goes clockwise or counterclockwise
     * @return the two-dimensional spiral array
     */
    private static int[][] generateSpiral(int height, int width, Corner startingPoint, Spin spin) {
        int[][] spiral = new int[height][width];
        int counter = 0;
        int i, j;
        Direction direction;
        //setting the values of i and j for the starting point and the initial direction
        //(either RIGHT, DOWN, LEFT or UP) depending on whether the spiral goes clockwise
        //or counterclockwise
        if (startingPoint.equals(Corner.TOP_LEFT)) {
            i = 0;
            j = 0;
            direction = spin.equals(Spin.CLOCKWISE) ? Direction.RIGHT : Direction.DOWN;
        } else if (startingPoint.equals(Corner.TOP_RIGHT)) {
            i = 0;
            j = width - 1;
            direction = spin.equals(Spin.CLOCKWISE) ? Direction.DOWN : Direction.LEFT;
        } else if (startingPoint.equals(Corner.BOTTOM_RIGHT)) {
            i = height - 1;
            j = width - 1;
            direction = spin.equals(Spin.CLOCKWISE) ? Direction.LEFT : Direction.UP;
        } else {
            i = height - 1;
            j = 0;
            direction = spin.equals(Spin.CLOCKWISE) ? Direction.UP : Direction.RIGHT;
        }
        spiral[i][j] = ++counter; //the starting point's value is set to 1
        while (counter < height * width) {
            int[] destination = calculateDestination(direction, i, j, height, width); /* the expected
             destination point's {i, j} coordinates depending on the current direction and position*/
            if (destination[0] == -1 || spiral[destination[0]][destination[1]] != 0) {
            //if either the destination point falls out of the array's boundaries or is already filled
            //with a non-zero value the direction needs to be changed according with the spin and the
            //destination point's {i, j} coordinates need to be recalculated
                direction = nextDirection(direction, spin);
                destination = calculateDestination(direction, i, j, height, width);
            }
            i = destination[0];
            j = destination[1];
            spiral[i][j] = ++counter;
        }
        return spiral;
    }

    /**
     * This method returns the next direction depending on the current direction and the spiral's spin
     *
     * @param currentDirection either {RIGHT, DOWN, LEFT, UP}
     * @param spin             either {CLOCKWISE, COUNTERCLOCKWISE}
     * @return either {RIGHT, DOWN, LEFT, UP}
     */
    private static Direction nextDirection(Direction currentDirection, Spin spin) {
        if (spin.equals(Spin.CLOCKWISE)) {
            switch (currentDirection) {
                case RIGHT:
                    return Direction.DOWN;
                case DOWN:
                    return Direction.LEFT;
                case LEFT:
                    return Direction.UP;
                case UP:
                    return Direction.RIGHT;
            }
        } else {
            switch (currentDirection) {
                case DOWN:
                    return Direction.RIGHT;
                case RIGHT:
                    return Direction.UP;
                case UP:
                    return Direction.LEFT;
                case LEFT:
                    return Direction.DOWN;
            }
        }
        return currentDirection; //unreachable statement to prevent compile errors
    }

    /**
     * Calculates the coordinates {i, j} of the element of the bi-dimensional array where you'd land if you move
     * one step away from the current element. If, depending on the direction and the starting position,
     * the movement would take you out of bounds, the method will return the array {-1, -1} to signal an impossible
     * movement
     *
     * @param currentDirection either {RIGHT, DOWN, LEFT, UP}
     * @param i                starting position's line index
     * @param j                starting position's column index
     * @param height           the two-dimensional array's amount of lines
     * @param width            the two-dimensional array's amount of columns
     * @return an array containing the destination point's {i, j} coordinates or {-1, -1} whether the move would take
     * you out of the array's bounds
     */
    private static int[] calculateDestination(Direction currentDirection, int i, int j, int height, int width) {
        switch (currentDirection) {
            case RIGHT:
                return offsetRight(i, j, width);
            case DOWN:
                return offsetDown(i, j, height);
            case LEFT:
                return offsetLeft(i, j);
            case UP:
                return offsetUp(i, j);
        }
        return new int[]{0, 0}; //unreachable statement to prevent compile errors
    }

    /**
     * Calculates the coordinates i, j of the element of the bi-dimensional array where you'd land if you move
     * one step right from the current element. The method will return the array {-1, -1} in case of an impossible
     * movement, depending on the current position and the two-dimensional array's width
     *
     * @param i     starting position's line index
     * @param j     starting position's column index
     * @param width the two-dimensional array's width
     * @return array containing the destination point's {i, j} coordinates or {-1, -1} whether the move would take
     * you out of the array's right bound
     */
    private static int[] offsetRight(int i, int j, int width) {
        return j == width - 1 ? new int[]{-1, -1} : new int[]{i, j + 1};
    }

    /**
     * Calculates the coordinates i, j of the element of the bi-dimensional array where you'd land if you move
     * one step left from the current element. The method will return the array {-1, -1} in case of an impossible
     * movement, depending on the current position
     *
     * @param i starting position's line index
     * @param j starting position's column index
     * @return array containing the destination point's {i, j} coordinates or {-1, -1} whether the move would take
     * you out of the array's left bound
     */
    private static int[] offsetLeft(int i, int j) {
        return j == 0 ? new int[]{-1, -1} : new int[]{i, j - 1};
    }

    /**
     * Calculates the coordinates i, j of the element of the bi-dimensional array where you'd land if you move
     * one step up from the current element. The method will return the array {-1, -1} in case of an impossible
     * movement, depending on the current position
     *
     * @param i starting position's line index
     * @param j starting position's column index
     * @return array containing the destination point's {i, j} coordinates or {-1, -1} whether the move would take
     * you out of the array's top bound
     */
    private static int[] offsetUp(int i, int j) {
        return i == 0 ? new int[]{-1, -1} : new int[]{i - 1, j};
    }

    /**
     * Calculates the coordinates i, j of the element of the bi-dimensional array where you'd land if you move
     * one step down from the current element. The method will return the array {-1, -1} in case of an impossible
     * movement, depending on the current position and the two-dimensional array's height
     *
     * @param i starting position's line index
     * @param j starting position's column index
     * @return array containing the destination point's {i, j} coordinates or {-1, -1} whether the move would take
     * you out of the array's lower bound
     */
    private static int[] offsetDown(int i, int j, int height) {
        return i == height - 1 ? new int[]{-1, -1} : new int[]{i + 1, j};
    }

    /**
     * Prints out the spiral elements. Columns will be separated by a space and all values will take the
     * same space, with left side padding if necessary. For instance, if the spiral has 100 elements,
     * all values will take 3 digits, with 1-digit values having 2 spaces padding on their left and 2-digit
     * values having 1 space padding on their left
     *
     * @param spiral the two-dimensional spiral array to be printed out
     */
    private static void printSpiral(int[][] spiral) {
        int digits = Integer.toString(spiral.length * spiral[0].length).length();
        for (int[] ints : spiral) {
            for (int anInt : ints) {
                System.out.printf("%" + digits + "d ", anInt);
            }
            System.out.println();
        }
    }
}
