package it.nesea.corsojava.modulo1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class QuadratiMagici {
    public static void main(String[] args) {
        int size = getSingleUnevenIntValueFromUser(3, 21);
        int[][] mySquare = generateMagicSquare(size);
        printMagicSquare(mySquare);
    }

    /**
     * This method generates and returns a Magic Square of a given size
     */
    private static int[][] generateMagicSquare(int size) {
        int[][] magicSquare = new int[size][size];
        int i = 0;        //the staring point is the middle of the first row, i.e.
        int j = size / 2; //the element having coordinates i=0 j=size/2
        int counter = 0;
        magicSquare[i][j] = ++counter; //the starting point's value is set to 1
        while (counter < size * size) {
            int[] destination = offsetNE(i, j, size); //our first option as destination
            //is the adjacent square in the north-east direction
            if (magicSquare[destination[0]][destination[1]] != 0) {
                //the North-East square is not empty, gotta go South and redefine
                //our destination
                destination = offsetS(i, j, size);
            }
            i = destination[0];
            j = destination[1];
            magicSquare[i][j] = ++counter;
        }
        return magicSquare;
    }

    /**
     * This method takes as input the coordinates of an element of a magic square and
     * returns an array containing the 2 coordinates of the adjacent element moving in
     * North-East direction. If the starting point is in the top row the destination
     * point is in the last row. If the starting point is in the last column the
     * destination point is in the first column
     */
    private static int[] offsetNE(int i, int j, int size) {
        int newI = i == 0 ? size - 1 : i - 1;
        int newJ = j == size - 1 ? 0 : j + 1;
        return new int[]{newI, newJ};
    }

    /**
     * This method takes as input the coordinates of an element of a magic square and
     * returns an array containing the 2 coordinates of the adjacent element moving in
     * South direction. If the starting point is in the bottom row the destination
     * point is in the first row
     */
    private static int[] offsetS(int i, int j, int size) {
        int newI = (i == size - 1) ? 0 : ++i;
        return new int[]{newI, j};
    }

    private static int getSingleUnevenIntValueFromUser(int minimum, int maximum) {
        int r = minimum - 1;
        Scanner in = new Scanner(System.in);
        System.out.print("Please give me an int uneven value not lower than " + minimum + " and not greater than " +
                maximum + ": ");
        while (r < minimum || r > maximum || r % 2 == 0) {
            try {
                r = in.nextInt();
                if (r < minimum || r > maximum) {
                    System.out.print("Value must be at least " + minimum + " and not greater than " +
                            maximum + "! Try again: ");
                }
                if (r % 2 == 0) {
                    System.out.print("Value must be UNEVEN, i.e. not a multiple of 2! " + r + " is an EVEN number. Try again" +
                            " (for example " + (r - 1) + " or " + (r + 1) + "): ");
                }
            } catch (InputMismatchException ex) {
                System.out.print("Value must be an integer! Try again: ");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                in = new Scanner(System.in);
            }
        }
        return r;
    }

    /**
     * This method takes an integer value as input and returns its amount of digits
     */
    private static int integerDigits(int input) {
        int r = 1;
        while (input / 10 > 0) {
            input /= 10;
            r++;
        }
        return r;
    }

    private static void printMagicSquare(int[][] square) {
        int size = square.length;
        int digits = integerDigits(size * size); //amount of digits of the integer
                                    //value corresponding to the square's amount of cells
        for (int[] arr : square) {
            for (int value : arr) {
                //every single element is printed with the same amount of digits
                //to guarantee the vertical alignment of the square's columns,
                //then a space is printed to guarantee horizontal separation
                System.out.printf("%" + digits + "d ", value);
            }
            System.out.println(); //carriage return at the end of the line
        }
    }

}
