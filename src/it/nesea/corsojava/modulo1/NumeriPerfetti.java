package it.nesea.corsojava.modulo1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class NumeriPerfetti {
    public static void main(String[] args) {
        System.out.println("Please give me a number between 1 and 1000000000");
        int m = getSingleIntValueFromUser(1, 1000000000);
        for (int i = 1; i < m; i++) {
            if (isPerfect(i)) {
                System.out.println(sumOfFactors(i));
            }
        }
    }

    public static int getSingleIntValueFromUser(int minimum, int maximum) {
        int r = 0;
        Scanner in = new Scanner(System.in);
        while (r < minimum || r > maximum) {
            try {
                r = in.nextInt();
                if (r < minimum || r > maximum) {
                    System.out.println("Value must be at least " + minimum + " and not greater than " +
                            maximum + "! Try again");
                }
            } catch (InputMismatchException ex) {
                System.out.println("Value must be an integer!");
                r = 0;
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                in = new Scanner(System.in);
            }
        }
        return r;
    }

    public static boolean isFactor(int numerator, int divisor) {
        return numerator % divisor == 0;
    }

    public static boolean isPerfect(int myValue) {
        int sumOfFactors = 0;
        for (int i = 1; i < (myValue / 2) + 1; i++) {
            if (isFactor(myValue, i)) {
                sumOfFactors += i;
            }
        }
        return myValue == sumOfFactors;
    }

    public static String sumOfFactors(int myValue) {
        String r = myValue + " = 1";
        for (int i = 2; i < myValue; i++) {
            if (isFactor(myValue, i)) {
                r += " + " + i;
            }
        }
        return r;
    }
}