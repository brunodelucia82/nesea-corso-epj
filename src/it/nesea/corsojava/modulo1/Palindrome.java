package it.nesea.corsojava.modulo1;

public class Palindrome {
    public static void main(String[] args) {
        String str1 = "anna";
        String str2 = "anno";
        String str3 = "abcdcba";
        String str4 = "";
        System.out.println("La stringa " + "\"" + str1 + "\"" + (isPalindrome(str1) ? " " : " non ") + "è palindroma");
        System.out.println("La stringa " + "\"" + str2 + "\"" + (isPalindrome(str2) ? " " : " non ") + "è palindroma");
        System.out.println("La stringa " + "\"" + str3 + "\"" + (isPalindrome(str3) ? " " : " non ") + "è palindroma");
        System.out.println("La stringa " + "\"" + str4 + "\"" + (isPalindrome(str4) ? " " : " non ") + "è palindroma");
    }

    public static boolean isPalindrome(String str) {
        for (int i = 0; i < str.length()/2 -1; i++) {
            if (str.charAt(i)!=str.charAt(str.length()-(1 + i))){
                return false;
            }
        }
        return true;
    }
}
