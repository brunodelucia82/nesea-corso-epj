package it.nesea.corsojava.modulo1;

import java.util.Scanner;

public class StringheVerticali {
    public static void main(String[] args) {
        String str1, str2, str3;
        Scanner in = new Scanner(System.in);
        System.out.println("Give me the first string please");
        str1 = in.nextLine();
        System.out.println("Give me the second string please");
        str2 = in.nextLine();
        System.out.println("Give me the third string please");
        str3 = in.nextLine();
        for (int i = 0; i < maxOfThree(str1.length(), str2.length(), str3.length()); i++) {
            printCharAtIfPossible(str1, i);
            printCharAtIfPossible(str2, i);
            printCharAtIfPossible(str3, i);
            System.out.println("");
        }
    }
    public static int maxOfThree(int a, int b, int c) {
        return Math.max(a, Math.max(b, c));
    }

    public static void printCharAtIfPossible(String myString, int myIndex) {
        if (myIndex < myString.length()) {
            System.out.print(myString.charAt(myIndex));
        } else {
            System.out.print(" ");
        }
    }
}