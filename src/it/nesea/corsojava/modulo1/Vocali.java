package it.nesea.corsojava.modulo1;

import java.util.Scanner;

public class Vocali {
    public static void main (String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please give me a string");
        String myString = in.nextLine();
        int aCount = 0, eCount = 0, iCount = 0, oCount = 0, uCount = 0;
        for (int i = 0; i < myString.length(); i++) {
            if (myString.charAt(i)=='a' || myString.charAt(i)=='A') {
                aCount++;
            } else if (myString.charAt(i)=='e' || myString.charAt(i)=='E') {
                eCount++;
            } else if (myString.charAt(i)=='i' || myString.charAt(i)=='I') {
                iCount++;
            } else if (myString.charAt(i)=='o' || myString.charAt(i)=='O') {
                oCount++;
            } else if (myString.charAt(i)=='u' || myString.charAt(i)=='U') {
                uCount++;
            }
        }
        System.out.println("a:" + aCount + " e:" + eCount + " i:" + iCount + " o:" + oCount + " u:" + uCount);
    }
}
