package it.nesea.corsojava.modulo1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TriplePitagoriche {
    public static void main(String[] args) {
        System.out.println("Please give me a number between 1 and 100");
        int m = getSingleIntValueFromUser(1, 100);
        for (int i = 1; i < m + 1; i++) {
            for (int j = i; j < m + 1; j++) {
                for (int k = j; k < m + 1; k++) {
                    if (isPythagoreanTriple(i, j, k)) {
                        System.out.println(i + ", " + j + ", and " + k + " represent a Pythagorean triple");
                    }
                }
            }
        }
    }

    public static boolean isPythagoreanTriple(int a, int b, int c) {
        if (a < 1 || b < 1 || c < 1) {
            throw new IllegalArgumentException("All values must be 1 or greater!");
        } else if (a > b || b > c) {
            throw new IllegalArgumentException("Arguments must be in non decreasing order");
        }
        return a * a + b * b == c * c;
    }

    public static int[] getSeveralIntValuesFromUser(int amount, int minimum, int maximum) {
        int[] triplet = new int[amount];
        Scanner in = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            while (triplet[i] < minimum) {
                System.out.print("Please give me value " + (char) (i + 97) + ": ");
                triplet[i] = getSingleIntValueFromUser(minimum, maximum);
            }
        }
        return triplet;
    }

    public static int getSingleIntValueFromUser(int minimum, int maximum) {
        int r = 0;
        Scanner in = new Scanner(System.in);
        while (r < minimum || r > maximum) {
            try {
                r = in.nextInt();
                if (r < minimum || r > maximum) {
                    System.out.println("Value must be at least " + minimum + " and not greater than " +
                            maximum + "! Try again");
                }
            } catch (InputMismatchException ex) {
                System.out.println("Value must be an integer!");
                r = 0;
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                in = new Scanner(System.in);
            }
        }
        return r;
    }

}
