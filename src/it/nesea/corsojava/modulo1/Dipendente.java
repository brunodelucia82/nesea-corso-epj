package it.nesea.corsojava.modulo1;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.lang.Character;
import java.util.Scanner;

public class Dipendente {
    private String firstName; //required
    private String lastName; //required
    private String placeOfBirth; //required
    private LocalDate dateOfBirth; //required
    private String fiscalCode; //required
    private double monthlySalary;
    private static double DEFAULT_MONTHLY_SALARY = 1200.00;
    private static double LEGAL_MINIMUM_MONTHLY_SALARY = 1000.00;
    private Contatti contatti = new Contatti();

    public Dipendente(String firstName, String lastName, String placeOfBirth, String dateOfBirthString, String fiscalCode) {
        try {
            setFirstName(firstName);
            setLastName(lastName);
            setPlaceOfBirth(placeOfBirth);
            setDateOfBirth(dateOfBirthString);
            setFiscalCode(fiscalCode);
        } catch (NullPointerException | IllegalArgumentException ex) {
            throw new IllegalArgumentException("Employee instance not created: one or more required fields were either " +
                    " missing or wrong");
        }
        this.monthlySalary = DEFAULT_MONTHLY_SALARY;
    }

    public Dipendente(String firstName, String lastName, String placeOfBirth, String dateOfBirthString, String fiscalCode,
                      double monthlySalary, String address, String email, String... phones) {

        this(firstName, lastName, placeOfBirth, dateOfBirthString, fiscalCode);
        try {
            setMonthlySalary(monthlySalary);
            this.contatti.setAddress(address);
            this.contatti.setEmail(email);
            for (int i = 0; i < phones.length; i++) {
                this.contatti.setPhoneNumber(phones[i], i + 1);
            }
        } catch (Exception ex) {
            System.out.println("The employee was created but one or more non required fields were either " +
                    "missing or wrong");
            ex.printStackTrace();
        }
    }

    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Sets the first name from an input String
     *
     * @param firstName the String representing the first name
     * @throws NullPointerException     if the input String is null
     * @throws IllegalArgumentException if either any character different from letters, spaces
     *                                  or apostrophes is found in the input String or the input String is empty
     */
    public void setFirstName(String firstName) {
        Objects.requireNonNull(firstName);
        if (firstName.length() == 0) {
            throw new IllegalArgumentException("First name cannot be an empty string!!!");
        }
        if (!validateName(firstName)) {
            throw new IllegalArgumentException("Unallowed characters found in the first name!");
        }
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    /**
     * Sets the last name from an input String
     *
     * @param lastName the String representing the last name
     * @throws NullPointerException     if the input String is null
     * @throws IllegalArgumentException if either any character different from letters, spaces
     *                                  or apostrophes is found in the input String or the input String is empty
     */
    public void setLastName(String lastName) {
        Objects.requireNonNull(lastName);
        if (lastName.length() == 0) {
            throw new IllegalArgumentException("Last name cannot be an empty string!!!");
        }
        if (!validateName(lastName)) {
            throw new IllegalArgumentException("Unallowed characters found in the last name!");
        }
        this.lastName = lastName;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public String getPlaceOfBirth() {
        return this.placeOfBirth;
    }

    /**
     * Sets the place of birth from an input String
     *
     * @param placeOfBirth the String representing the place of birth
     * @throws NullPointerException     if the input String is null
     * @throws IllegalArgumentException if either any character different from letters, spaces
     *                                  or apostrophes is found in the input String or the input String is empty
     */
    public void setPlaceOfBirth(String placeOfBirth) {
        Objects.requireNonNull(placeOfBirth, "Place of birth can't be null!");
        if (placeOfBirth.length() == 0) {
            throw new IllegalArgumentException("Place of birth cannot be an empty string!!!");
        }
        if (!validateName(placeOfBirth)) {
            throw new IllegalArgumentException("Unallowed characters found in the place of birth!");
        }
        this.placeOfBirth = placeOfBirth;
    }

    public LocalDate getDateOfBirth() {
        return this.dateOfBirth;
    }

    /**
     * Sets the date of birth from a String input. The date must be in the DD/MM/YYYY format
     *
     * @param dateOfBirthString the String representing the date of birth in the DD/MM/YYYY format
     * @throws NullPointerException     if the input String is null
     * @throws IllegalArgumentException if the String doesn't match the DD/MM/YYYY format or the values are out of the 1-12 range
     *                                  for months and 1-last day of month range for days
     */
    public void setDateOfBirth(String dateOfBirthString) {
        Objects.requireNonNull(dateOfBirthString, "Date of birth cannot be null!");
        Scanner process = new Scanner(dateOfBirthString).useDelimiter("/");
        int dayOfMonth = 0;
        int month = 0;
        int year = 0;
        try {
            dayOfMonth = process.nextInt();
            month = process.nextInt();
            year = process.nextInt();
        } catch (NoSuchElementException ex) {
            throw new IllegalArgumentException("Invalid date format: make sure the date is in the DD/MM/YYYY format!");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        LocalDate dateOfBirth;
        try {
            dateOfBirth = LocalDate.of(year, month, dayOfMonth);
        } catch (DateTimeException ex) {
            throw new IllegalArgumentException("Invalid date of birth: make sure that the day is between 1 and the last " +
                    "day of month and the month is between 1 and 12");
        }
        this.dateOfBirth = dateOfBirth;
    }

    public double getMonthlySalary() {
        return this.monthlySalary;
    }

    /**
     * Sets the monthly salary
     *
     * @param monthlySalary a double representing the monthly salary. The value will be rounded automatically
     *                      to the second decimal place
     * @throws IllegalArgumentException If the value of monthlySalary is lower than the LEGAL_MINIMUM_MONTHLY_SALARY
     */
    public void setMonthlySalary(double monthlySalary) {
        if (monthlySalary < LEGAL_MINIMUM_MONTHLY_SALARY) {
            throw new IllegalArgumentException("Slavery is not allowed");
        }
        this.monthlySalary = Math.round(monthlySalary * 100) / 100.0;
    }

    public String getFiscalCode() {
        return this.fiscalCode;
    }

    /**
     * Sets the fiscal code from an input String
     *
     * @param s the String representing the fiscal code
     * @throws NullPointerException     if the input String is null
     * @throws IllegalArgumentException if the String is not a valid fiscal code
     */
    public void setFiscalCode(String s) {
        Objects.requireNonNull(s, "Fiscal code can't be null");
        s = s.toUpperCase();
        if (!validateFiscalCodeCIN(s)) {
            throw new IllegalArgumentException("This fiscal code is wrong");
        }
        this.fiscalCode = s;
    }

    /**
     * Checks if the last digit of the fiscal code is coherent, i.e. if the last letter is correct according
     * with the fiscal code generation algorithm applied in Italy
     *
     * @param s the String representing a fiscal code
     * @return true if the last letter id correct, false if the last letter is wrong or the String's length is
     * not 16
     */
    public boolean validateFiscalCodeCIN(String s) {
        if (s.length() != 16) {
            return false;
        }
        char[][] valuesElementsMap = new char[][]{
                {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                        'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'},
                {1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 1, 0, 5, 7, 9, 13, 15, 17,              //values for uneven
                        19, 21, 2, 4, 18, 20, 11, 3, 6, 8, 12, 14, 16, 10, 22, 25, 24, 23}, //elements
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7,                          //values for even
                        8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25}   //elements
        };
        s = s.toUpperCase();
        int cin = 0;
        for (int i = 0; i < s.length() - 1; i++) {
            char c = s.charAt(i);
            int column = findChar(c, valuesElementsMap[0]);
            if (column == -1) {
                return false;
            }
            if (i % 2 == 0) { //the alphanumeric elements of the fiscal code are 1-based, therefore
                //we pick values for uneven elements when i is an even value and viceversa
                cin += (int) valuesElementsMap[1][column];
            } else {
                cin += (int) valuesElementsMap[2][column];
            }
        }
        cin %= 26;
        cin += 'A';
        char cinCharacter = (char) cin;
        return s.charAt(s.length() - 1) == cinCharacter;
    }

    /**
     * Returns the index of the first occurrence of a char ch in an array of chars
     *
     * @param ch    the char you want to look for
     * @param chars the array of chars which is being searched
     * @return an int representing the index of the first occurrence of ch, -1 if ch wasn't found
     */
    private int findChar(char ch, char[] chars) {
        for (int i = 0; i < chars.length; i++) {
            if (ch == chars[i]) {
                return i;
            }
        }
        return -1;
    }

    public Contatti getContatti() {
        return this.contatti;
    }

    /**
     * Validate first or last name by checking that it only contains either letters, spaces or apostrophe (')
     *
     * @param s the String representing a first name or last name
     * @return boolean true if the String meets the requirements or false if it doesn't
     */
    private boolean validateName(String s) {
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (!Character.isLetter(c) && c != '\'' && c != ' ') {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder r = new StringBuilder();
        r.append("employee ").append(getFullName().toUpperCase()).append("\n");
        r.append("personal details").append("\n");
        r.append("first name: ").append(getFirstName()).append("\n");
        r.append("last name: ").append(getLastName()).append("\n");
        r.append("date of birth: ").append(getDateOfBirth()).append("\n");
        r.append("place of birth: ").append(getPlaceOfBirth()).append("\n");
        r.append("fiscal code: ").append(getFiscalCode()).append("\n");
        r.append("monthly salary: ").append(getMonthlySalary()).append("\n");
        r.append(this.contatti.toString());
        return r.toString();
    }

    public class Contatti {
        private String address = "";
        private String email = "";
        private String[] phoneNumbers = new String[5];

        private Contatti() {
            for (int i = 0; i < this.phoneNumbers.length; i++) {
                this.phoneNumbers[i] = "";
            }
        }

        public String getAddress() {
            return this.address.equals("") ? "unknown" : address;
        }

        public void setAddress(String s) {
            this.address = Objects.requireNonNull(s, "the address can't be null");
        }

        public String getEmail() {
            return email.equals("") ? "unknown" : email;
        }

        /**
         * Sets the email address in lowercase
         *
         * @param email a String representing the email address
         * @throws NullPointerException if the String email is null
         */
        public void setEmail(String email) {
            Objects.requireNonNull(email);
            this.email = email.toLowerCase();
        }

        /**
         * Returns the telephone number stored in the slot represented by the parameter i
         *
         * @param i the required slot number, ranging 1 to 5
         * @return the String representing the telephone number in the desired slot or "N/A"
         * if an empty String is stored in the slot number i
         * @throws IllegalArgumentException if the index i is lower than 1 or greater than 5
         */
        public String getPhoneNumber(int i) {
            if (i < 0 || i > this.phoneNumbers.length - 1) {
                throw new IllegalArgumentException("position must be a value between 1 and " + this.phoneNumbers.length);
            }
            return this.phoneNumbers[--i].equals("") ? "N/A" : this.phoneNumbers[i];
        }

        /**
         * Returns the telephone number stored in the last non empty slot
         *
         * @return the String representing the telephone number stored in the last non empty slot
         */
        public String getPhoneNumber() {
            return this.phoneNumbers[firstEmptyPhoneNumberSlot() - 1];
        }

        /**
         * Sets a phone number
         *
         * @param s the string representing a telephone number
         * @param i an index (1 to 5) representing the slot where the phone number will be stored.
         *          If the phone number is already stored the input won't be stored again and a warning will be printed out
         *          IMPORTANT: if there's any empty slot at an index lower than i the phone number will be stored in the
         *          first available empty slot and the value of i will be ignored.
         * @throws NullPointerException     if the String s is null
         * @throws IllegalArgumentException if either the String contains anything besides digits and spaces or
         *                                  there are no empty slots and the value of the index i is either
         *                                  lower than 1 or greater than 5
         */
        public void setPhoneNumber(String s, int i) {
            Objects.requireNonNull(s);
            if (!validatePhoneNumber(s)) {
                throw new IllegalArgumentException("Phone number can only contain digits and spaces");
            }
            int pos = searchPhoneNumber(s);
            if (pos > -1) {
                System.out.println("This telephone number (" + s + ") is already stored in the slot number " + ++pos);
                return;
            }
            i = firstEmptyPhoneNumberSlot() < (i - 1) ? firstEmptyPhoneNumberSlot() : --i;
            if (i < 0 || i > this.phoneNumbers.length - 1) {
                throw new IllegalArgumentException("position must be a value between 1 and " + this.phoneNumbers.length);
            }
            this.phoneNumbers[i] = s;
        }

        /**
         * Validates phone number by checking that the String contains nothing but numeric digits
         * and spaces
         *
         * @param s the input String representing a phone number
         * @return boolean true or false
         */
        private boolean validatePhoneNumber(String s) {
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                if (!Character.isDigit(c) && c != ' ') {
                    return false;
                }
            }
            return true;
        }

        /**
         * Normalizes a String representing a phone number by removing all spaces and keeping only the numeric digits,
         * allowing comparision of different phone numbers
         *
         * @param s a String representing a phone number
         * @return the input String without spaces
         */
        private String normalizePhoneNumber(String s) {
            StringBuilder r = new StringBuilder();
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                if (Character.isDigit(c)) {
                    r.append(c);
                }
            }
            return r.toString();
        }

        private int firstEmptyPhoneNumberSlot() {
            int r = -1;
            for (int i = 0; i < this.phoneNumbers.length; i++) {
                if (this.phoneNumbers[i].equals("")) {
                    return i;
                }
            }
            return r;
        }

        private int searchPhoneNumber(String input) {
            int r = -1;
            int i = 0;
            String normalizedInput = normalizePhoneNumber(input);
            for (String s : this.phoneNumbers) {
                if (normalizePhoneNumber(s).equals(normalizedInput)) return i;
                i++;
            }
            return r;
        }

        @Override
        public String toString() {
            StringBuilder r = new StringBuilder();
            r.append("contact details\n");
            r.append("address: ").append(getAddress()).append("\n");
            r.append("email: ").append(getEmail()).append("\n");
            r.append("phone numbers");
            int phonesCeiling = firstEmptyPhoneNumberSlot();
            if (phonesCeiling == 0) {
                r.append(": none available");
            } else {
                r.append("\n");
                for (int i = 0; i < phonesCeiling; i++) {
                    r.append("phone ").append((i + 1)).append(": ").append(phoneNumbers[i]).append("\n");
                }
            }
            return r.toString();
        }
    }

}
