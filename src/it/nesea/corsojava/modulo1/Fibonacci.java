package it.nesea.corsojava.modulo1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Fibonacci {
    public static void main(String[] args) {
        final int MAX = 100;
        System.out.print("Please give me a positive integer not greater than " + MAX + ": ");
        int n = getSingleIntValueFromUser(1, MAX);
        System.out.println("Fibonacci's element number " + n + " is " + nThFibonacci(n));
    }

    private static int getSingleIntValueFromUser(int minimum, int maximum) {
        int r = 0;
        Scanner in = new Scanner(System.in);
        while (r < minimum || r > maximum) {
            try {
                r = in.nextInt();
                if (r < minimum || r > maximum) {
                    System.out.print("Value must be at least " + minimum + " and not greater than " +
                            maximum + "! Try again: ");
                }
            } catch (InputMismatchException ex) {
                System.out.print("Value must be an integer! Try again: ");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                in = new Scanner(System.in);
            }
        }
        return r;
    }

    private static int nThFibonacci(int n) {
        if (n == 1 || n == 2) {
            return 1;
        } else {
            return nThFibonacci(n - 2) + nThFibonacci(n - 1);
        }
    }
}
