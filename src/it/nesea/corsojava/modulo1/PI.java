package it.nesea.corsojava.modulo1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class PI {
    public static void main(String[] args) {
        long myValue = getSingleLongValueFromUser(1L, (long) Math.pow(10, 16));
        System.out.println("The sum of the series at the " + myValue + ordinalSuffix(myValue) + " element is "
                + kElementSum(myValue));
        int accuracy = getSingleIntValueFromUser(0, 8);
        long start = System.currentTimeMillis();
        myValue = kForNDigitsAccuracy(accuracy);
        long finish = System.currentTimeMillis();
        System.out.println("At the " + myValue + ordinalSuffix(myValue) + " element the sum of the series is "
                + kElementSum(myValue) + ", first value having " + accuracy + " decimal digits correct" +
                " (PI=" + Math.PI + ")");
        System.out.println("It took " + (finish - start) + " milliseconds to calculate this");
    }

    private static int getSingleIntValueFromUser(int minimum, int maximum) {
        int r = minimum - 1;
        Scanner in = new Scanner(System.in);
        System.out.print("Please give me an int value not lower than " + minimum + " and not greater than " +
                maximum + ": ");
        while (r < minimum || r > maximum) {
            try {
                r = in.nextInt();
                if (r < minimum || r > maximum) {
                    System.out.print("Value must be at least " + minimum + " and not greater than " +
                            maximum + "! Try again: ");
                }
            } catch (InputMismatchException ex) {
                System.out.print("Value must be an integer! Try again: ");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                in = new Scanner(System.in);
            }
        }
        return r;
    }

    private static long getSingleLongValueFromUser(long minimum, long maximum) {
        long r = minimum - 1;
        Scanner in = new Scanner(System.in);
        System.out.print("Please give me a long value not lower than " + minimum +
                " and not greater than " + maximum + ": ");
        while (r < minimum || r > maximum) {
            try {
                r = in.nextLong();
                if (r < minimum || r > maximum) {
                    System.out.print("Value must be at least " + minimum + " and not greater than " +
                            maximum + "! Try again: ");
                }
            } catch (InputMismatchException ex) {
                System.out.print("Value must be a long! Try again: ");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                in = new Scanner(System.in);
            }
        }
        return r;
    }

    private static double kElement(long k) {
        return -1 * Math.pow(-1, k) * (4.0 / ((double) (2 * k - 1)));
    }

    private static double kElementSum(long k) throws IllegalArgumentException {
        if (k < 1) {
            throw new IllegalArgumentException("Value must be greater than or equal to 1!");
        }
        double r = 0D;
        for (long i = 1; i < k + 1; i++) {
            r += kElement(i);
        }
        return r;
    }

    private static int kForNDigitsAccuracy(int accuracy) {
        System.out.println("Calculating. This might take a while...");
        double multiplier = Math.pow(10, accuracy);
        int matcher = (int) (Math.PI * multiplier);
        double sum = 0D;
        int i = 0;
        int difference = -1;
        while (difference != 0) {
            sum += kElement(++i);
            difference = ((int) (sum * multiplier)) - matcher;
        }
        return i;
    }

    private static String ordinalSuffix(long myValue) throws IllegalArgumentException {
        long lastDigit;
        if (myValue < 1) {
            throw new IllegalArgumentException("Value must be greater than or equal to 1!");
        } else {
            lastDigit = myValue % 10;
        }
        if (lastDigit == 1) {
            return "st";
        } else if (lastDigit == 2) {
            return "nd";
        } else if (lastDigit == 3) {
            return "rd";
        } else {
            return "th";
        }
    }
}
