package it.nesea.corsojava.modulo1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Fattorizzazione {
    public static void main(String[] args) {
        System.out.println("Please give me a long value:");
        long n = getSingleLongValueFromUser(2, Long.MAX_VALUE);
        System.out.println(factorize(n));
    }

    public static long getSingleLongValueFromUser(long minimum, long maximum) {
        long r = 0;
        Scanner in = new Scanner(System.in);
        while (r < minimum || r > maximum) {
            try {
                r = in.nextLong();
                if (r < minimum || r > maximum) {
                    System.out.println("Value must be at least " + minimum + " and not greater than " +
                            maximum + "! Try again");
                }
            } catch (InputMismatchException ex) {
                System.out.println("Value must be a long!");
                r = 0;
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                in = new Scanner(System.in);
            }
        }
        return r;
    }

    public static boolean isPrime(long value) {
        for (long i = 2; i < (value / 2) + 1; i++) {
            if (value % i == 0) return false;
        }
        return true;
    }

    public static boolean isFactor(long numerator, long divisor) {
        return numerator % divisor == 0;
    }

    public static boolean isPrimeFactor(long numerator, long divisor) {
        return isFactor(numerator, divisor) && isPrime(divisor);
    }

    public static String factorize(long value) {
        String r = "";
        for (long i = 2; i < value + 1; i++) {
            if (isPrimeFactor(value, i)) {
                r += i;
                long pow = 0;
                long dividend = value;
                while (dividend % i == 0) {
                    pow++;
                    dividend /= i;
                }
                r += pow > 1 ? "^" + pow + " " : " ";
            }
        }
        if (r.endsWith(" ")) {
            return r.substring(0, r.length()-1);
        } else {
            return r;
        }
    }

}