package it.nesea.corsojava.modulo1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Monete {
    public static void main(String[] args) {
        System.out.println("Dammi un valore:");
        int amount = getSingleIntValueFromUser(1, 1000000);
        System.out.print(coinsBreakdown(amount));
    }

    private static int getSingleIntValueFromUser(int minimum, int maximum) {
        int r = 0;
        Scanner in = new Scanner(System.in);
        while (r < minimum || r > maximum) {
            try {
                r = in.nextInt();
                if (r < minimum || r > maximum) {
                    System.out.println("Value must be at least " + minimum + " and not greater than " +
                            maximum + "! Try again");
                }
            } catch (InputMismatchException ex) {
                System.out.println("Value must be an integer!");
                r = 0;
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                in = new Scanner(System.in);
            }
        }
        return r;
    }

    private static String monetaMonete(int value) {
        return value == 1 ? "moneta" : "monete";
    }

    private static String coinsBreakdown(int value) {
        String r = "";
        int quantity = value / 50;
        if (quantity > 0) {
            r += quantity + " " + monetaMonete(quantity) + " da 50" + "\n";
        }
        value %= 50;
        quantity = value / 20;
        if (quantity > 0) {
            r += quantity + " " + monetaMonete(quantity) + " da 20" + "\n";
        }
        value %= 20;
        quantity = value / 10;
        if (quantity > 0) {
            r += quantity + " " + monetaMonete(quantity) + " da 10" + "\n";
        }
        value %= 10;
        quantity = value / 5;
        if (quantity > 0) {
            r += quantity + " " + monetaMonete(quantity) + " da 5" + "\n";
        }
        value %= 5;
        quantity = value / 2;
        if (quantity > 0) {
            r += quantity + " " + monetaMonete(quantity) + " da 2" + "\n";
        }
        value %= 2;
        quantity = value;
        if (quantity > 0) {
            r += quantity + " " + monetaMonete(quantity) + " da 1" + "\n";
        }
        return r;
    }

}