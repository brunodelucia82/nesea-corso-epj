package it.nesea.epj.modulo1;

import java.util.Objects;

class Main {
    public static void main(String args[]) {
        // con campi private
        Dipendente riccardo = new Dipendente("Riccardo", 3000);
        Dipendente bruno = new Dipendente("Bruno", 3000);
        System.out.println(riccardo.getStipendio());
        System.out.println(bruno.getStipendio());
        System.out.println("Il codice di bruno e' " + bruno.getCodice());
        System.out.println("Il codice di riccardo e' " + riccardo.getCodice());
        // istanza poverello, prende lo stipendio default ovvero 1000
        Dipendente poverello = new Dipendente("Dipendente poverello", 100);
        System.out.println("Poverello guadagna " + poverello.getStipendio());
        // promuovo dipendente poverello
        poverello.setStipendio(2000);
        System.out.println("Dopo la promozione Poverello guadagna " + poverello.getStipendio());

        // sta a significare che questa condizione e' sempre vera!!!
        assert (bruno.getNomeCognome() != null); // non puo mai essere null
        // con campi public
        DipendentePublic brunoPublic = new DipendentePublic();
        System.out.println("Può essere null? " + (brunoPublic.nomeCognome == null)); // puo essere null?
        brunoPublic.nomeCognome = "Bruno";
        brunoPublic.stipendio = 3000;
        // nel costruttore posso verificare la correttezza dei parametri passati
        try {
            new Dipendente(null, 3000);
        } catch (IllegalArgumentException e) {
            System.out.println("Hai costruito un dipendente senza nome");
        }
        // Immutabilita'
        DipendenteImmutabile simone = new DipendenteImmutabile("Simone", 3000);
        // metodo preso dalle dispense
        stampaDipendenti();
    }

    private static void stampaDipendenti() {
        Dipendente rossi = new Dipendente("Mario Rossi", 2000.0);
        rossi.setIndirizzo("Roma, via Rossini, 15");
        rossi.setTelefono("06 8989898");
        Dipendente verdi = new Dipendente("Ugo Verdi", 1850.0);
        verdi.setIndirizzo("Roma, via G. Verdi, 30");
        for (Dipendente d : new Dipendente[]{rossi, verdi}) {
            System.out.print("Dipendente: " + d.getNomeCognome());
            System.out.println("  codice: " + d.getCodice());
            System.out.println("    stipendio:  " + d.getStipendio());
            Dipendente.Contatti con = d.getContatti();
            System.out.println("    Indirizzo: " + con.getIndirizzo());
            System.out.println("    Telefono: " + con.getTelefono());
        }
    }

}

/**
 * Un oggetto {@code Dipendente} rappresenta un dipendente dell'azienda
 */
public class Dipendente implements Comparable {
    private String nomeCognome;
    private double stipendio = 1000.0;
    private final long codice;
    private Contatti contatti;
    private Dipendente supervisore;

    private static long ultimoCodice;     // Ultimo codice usato

    /**
     * Crea un dipendente con i dati specificati.
     *
     * @param nomeCognome nome e cognome del dipendente
     * @param stipendio   stipendio del dipendente
     */
    public Dipendente(String nomeCognome, double stipendio) {
        // assegnazione stipendio, uso il setter invece di
        // if (stipendio < 0) throw new IllegalArgumentException("Lo stipendio deve essere un numero positivo");
        // this.stipendio = stipendio;
        setStipendio(stipendio);
        // assegna nome cognome
        setNomeCognome(nomeCognome);
        // assegna un codice al dipendente
        this.codice = nuovoCodice();
        // inizializzo un oggetto di tipo contatti "vuoto"
        contatti = new Contatti();
    }

    public Dipendente(String nomeCognome) {
        this(nomeCognome, 10);
    }

    public String getNomeCognome() {
        return nomeCognome;
    }

    public void setNomeCognome(String nomeCognome) {
        if (nomeCognome == null) throw new IllegalArgumentException("Il nome non puo' essere null");
        this.nomeCognome = nomeCognome;
    }

    public double getStipendio() {
        return stipendio;
    }

    public void setStipendio(double stipendio) {
        if (stipendio < 0) throw new IllegalArgumentException("Lo stipendio deve essere un numero positivo");
        this.stipendio = stipendio;
    }

    /**
     * @return il supervisore di questo dipendente
     */
    public Dipendente getSupervisore() {
        return supervisore;
    }

    /**
     * Imposta il supervisore di questo dipendente.
     *
     * @param s il supervisore
     */
    public void setSupervisore(Dipendente s) {
        supervisore = s;
    }

    /**
     * @return i contatti di questo dipendente
     */
    public Contatti getContatti() {
        return contatti;
    }

    /**
     * Imposta l'indirizzo di questo dipendente.
     *
     * @param indirizzo il nuovo indirizzo
     * @throws NullPointerException se indirizzo è null
     */
    public void setIndirizzo(String indirizzo) {
        if (indirizzo == null)
            throw new NullPointerException("Indirizzo non può essere null");
        contatti.indirizzo = indirizzo;
    }

    public void setIndirizzoConRequireNotNull(String indirizzo) {
        contatti.indirizzo = Objects.requireNonNull(indirizzo, "Indirizzo non può essere null");
    }

    /**
     * Imposta il recapito telefonico di questo dipendente.
     *
     * @param telefono il nuovo numero di telefono
     */
    public void setTelefono(String telefono) {
        contatti.telefono = telefono;
    }

    /**
     * @return il codice di questo dipendente
     */
    public final long getCodice() {
        return codice;
    }

    private static long nuovoCodice() {   // Ritorna un nuovo codice
        return ++ultimoCodice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dipendente)) return false;
        Dipendente that = (Dipendente) o;
        // comparo solamente codice perche' assumo che il codice mi identifichi
        // univocamente un dipendente
        return codice == that.codice;
    }

    @Override
    public String toString() {
        return "Dipendente{" +
                "nomeCognome='" + nomeCognome + '\'' +
                ", codice=" + codice +
                ", stipendio=" + stipendio +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        long otherCodice = ((Dipendente) o).codice;
        if (codice > otherCodice) {
            return 1;
        } else {
            return codice == otherCodice ? 0 : -1;
        }
    }

    /**
     * Mantiene i contatti di un dipendente come indirizzo, telefono, ecc.
     */
    public static class Contatti {
        @Override
        public String toString() {
            return "Contatti{" +
                    "indirizzo='" + indirizzo + '\'' +
                    ", telefono='" + telefono + '\'' +
                    '}';
        }

        /**
         * @return l'indirizzo del dipendente
         */
        public String getIndirizzo() {
            return indirizzo;
        }

        /**
         * @return il recapito telefonico del dipendente
         */
        public String getTelefono() {
            return telefono;
        }

        private Contatti() {
            indirizzo = "";
            telefono = "";
        }

        private String indirizzo;
        private String telefono;

    }
}

class DipendentePublic {
    public String nomeCognome; // prende valori di default ovvero null
    public double stipendio; // prende il valore di default ovvero 0.0
}

class DipendenteImmutabile {
    private String nomeCognome;
    private double stipendio = 1000.0;

    /**
     * Crea un dipendente con i dati specificati.
     *
     * @param nomeCognome nome e cognome del dipendente
     * @param stipendio   stipendio del dipendente
     */
    public DipendenteImmutabile(String nomeCognome, double stipendio) {
        // assegnazione stipendio, uso il setter invece di
        // if (stipendio < 0) throw new IllegalArgumentException("Lo stipendio deve essere un numero positivo");
        // this.stipendio = stipendio;
        setStipendio(stipendio);
        // assegna nome cognome
        setNomeCognome(nomeCognome);
    }

    public DipendenteImmutabile(String nomeCognome) {
        setNomeCognome(nomeCognome);
    }

    public String getNomeCognome() {
        return nomeCognome;
    }

    private void setNomeCognome(String nomeCognome) {
        if (nomeCognome == null) throw new IllegalArgumentException("Il nome non puo' essere null");
        this.nomeCognome = nomeCognome;
    }

    public double getStipendio() {
        return stipendio;
    }

    private void setStipendio(double stipendio) {
        if (stipendio < 0) throw new IllegalArgumentException("Lo stipendio deve essere un numero positivo");
        this.stipendio = stipendio;
    }
}

class DipendenteImmutabileFinal {
    /**
     * Lo stipendio di default quando un dipendente non ha un trattamento particolare
     */
    private static final double STIPENDIO_DEFAULT = 1000;
    private final String nomeCognome;
    private final double stipendio;

    /**
     * Crea un dipendente con i dati specificati.
     *
     * @param nomeCognome nome e cognome del dipendente
     * @param stipendio   stipendio del dipendente
     */
    public DipendenteImmutabileFinal(String nomeCognome, double stipendio) {
        // tutti gli altri costruttori sono costretti a passare per questo, pertanto
        // inserendo i controlli sulle variabili in questo punto evito di ripeterli
        if (nomeCognome == null || "".equals(nomeCognome))
            throw new IllegalArgumentException("Il nome non puo' essere null");
        if (stipendio < 0) throw new IllegalArgumentException("Lo stipendio deve essere un numero positivo");
        this.nomeCognome = nomeCognome;
        this.stipendio = stipendio;
        System.out.println("Sono a riga 162");
    }

    public DipendenteImmutabileFinal(String nomeCognome) {
        this(nomeCognome, STIPENDIO_DEFAULT);
        System.out.println("Sono a riga 167");
    }

    public DipendenteImmutabileFinal() {
        // invoca con il meccanismo dell'overloading (tra costruttori)
        // il costruttore della stessa classe che prende una stringa
        this("Anonymous");
        System.out.println("Sono a riga 174");
    }

    public static void main(String args[]) {
        new DipendenteImmutabileFinal();
    }
}