package it.nesea.epj.modulo1.menu.abstractmenu;

/**
 * @author Fabio Napoleoni
 */

import java.util.Arrays;
import java.util.Scanner;

/**
 * {@code MenuApp} fornisce le funzionalità di base per un'applicazione basata
 * su un menu testuale. La sotto-classe deve fornire i contenuti.
 */
public abstract class MenuApp {
    private int menuWidth;

    /**
     * Esegue l'applicazione. L'utente sceglie una voce del menu digitando il
     * numero mostrato a sinistra della voce.
     */
    public void run(boolean frame) {
        int n = menu.length;
        Scanner input = new Scanner(System.in);
        boolean quit = false;
        while (!quit) {
            if (frame){
                System.out.println(concatAsterisks(menuWidth));
            }
            for (int i = 0; i < n; i++) {
                if (frame){
                    System.out.print("*  ");
                }
                System.out.printf("%" + Integer.toString(menu.length).length() + "d. %s", i + 1, menu[i]);
                if (frame) {
                    System.out.print(concatSpaces(widestMenuElement() - menu[i].length()) + "  *");
                }
                System.out.println();
            }
            if (frame){
                System.out.println(concatAsterisks(menuWidth));
            }
            while (!input.hasNextInt())
                input.next();      // Scarta qualsiasi input che non è un intero
            int choice = input.nextInt();
            if (choice >= 1 && choice < n) doMenu(choice);
            else if (choice == n) quit = true;
        }
        System.out.println("Applicazione terminata");
    }

    /**
     * Usato dalla sotto-classe per inizializzare le voci del menu, esclusa la
     * voce per terminare "Quit", che è gestita direttamente da questa classe.
     *
     * @param items le voci del menu
     */
    protected MenuApp(String... items) {
        menu = Arrays.copyOf(items, items.length + 1);
        menu[menu.length - 1] = "Quit";
        menuWidth = widestMenuElement() + 8 + Integer.toString(menu.length).length();
    }

    protected int widestMenuElement() {
        int r = 0;
        for (String s : menu) {
            r = s.length() > r ? s.length() : r;
        }
        return r;
    }

    protected String concatAsterisks(int width) {
        StringBuilder r = new StringBuilder();
        for (int i = 0; i < width; i++) {
            r.append("*");
        }
        return r.toString();
    }

    protected String concatSpaces(int width) {
        StringBuilder r = new StringBuilder();
        for (int i = 0; i < width; i++) {
            r.append(" ");
        }
        return r.toString();
    }

    protected void metodoProtetto() {

    }

    void metodoPackageProtected() {

    }

    public void metodoEreditatoPublic() {
    }

    protected void metodoEreditatoProtected() {
    }

    /**
     * Implementato dalla sotto-classe per eseguire la voce del menu scelta.
     *
     * @param choice il numero della voce di menu scelta
     */
    protected abstract void doMenu(int choice);

    private final String[] menu;
}