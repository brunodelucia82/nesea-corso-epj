package it.nesea.epj.modulo1.menu.azienda;

import it.nesea.epj.modulo1.Dipendente;
import it.nesea.epj.modulo1.interfacce.Checker;
import it.nesea.epj.modulo1.menu.abstractmenu.MenuApp;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author Fabio Napoleoni
 */
public class AziendaApp extends MenuApp {

    private static final Checker CHECK_NOMECOGNOME = new CheckNomeCognome();
    private static final Checker CHECK_INDIRIZZO = new CheckIndirizzo();
    private static final Checker CHECK_TELEFONO = new CheckTelefono();
    private static final Checker CHECK_CODICE = new CheckCodice();
    private static final Checker CHECK_STIPENDIO = new CheckStipendio();

    public static void main(String[] args) {
        AziendaApp app = new AziendaApp();
        app.run(false);
    }

    public AziendaApp() {
        super("Nuovo...", "Cerca...", "Rimuovi...", "Tutti");
        dipendenti = new Dipendente[0];
    }

    /**
     * Legge dallo {@link java.util.Scanner} {@code input} una stringa finché non
     * passa il controllo del {@link Checker} {@code check}.
     *
     * @param input  scanner da cui leggere
     * @param prompt la descrizione della stringa da leggere
     * @param check  il controllo
     * @return la prima stringa letta che passa il controllo
     */
    private static String inputString(Scanner input, String prompt, Checker check) {
        while (true) {
            System.out.print(prompt);
            String s = input.nextLine();
            String r = check.valid(s);
            if (r != null)
                System.err.println("ERRORE: " + r);
            else
                return s;
        }
    }

    @Override
    protected void doMenu(int choice) {
        switch (choice) {
            case 1:
                nuovo();
                break;     // Aggiunge un dipendente
            case 2:
                cerca();
                break;     // Cerca un dipendente
            case 3:
                rimuovi();
                break;   // Rimuove un dipendente
            case 4:
                tutti();
                break;     // Stampa tutti i dipendenti
        }
    }

    /**
     * Legge dalla console i dati di un nuovo dipendente e lo aggiunge
     * all'archivio.
     */
    private void nuovo() {
        Scanner input = new Scanner(System.in);
        String nc = inputString(input, "Nome e cognome: ", CHECK_NOMECOGNOME);
        String ind = inputString(input, "Indirizzo: ", CHECK_INDIRIZZO);
        String tel = inputString(input, "Telefono: ", CHECK_TELEFONO);
        Double stipendio = -1.0;
        while (stipendio < 0) {
            try {
                stipendio = Double.parseDouble(inputString(input, "Stipendio: ", CHECK_STIPENDIO));
            } catch (NumberFormatException e) {
                System.err.println("Formato numerico errato, controllare il corretto uso di punti/virgole per separare i decimali");
            }
        }
        Dipendente d = new Dipendente(nc);
        d.setIndirizzo(ind);
        d.setTelefono(tel);
        d.setStipendio(stipendio);
        dipendenti = Arrays.copyOf(dipendenti, dipendenti.length + 1);
        dipendenti[dipendenti.length - 1] = d;
        System.out.println("Il dipendente " + nc + " è stato inserito");
    }

    /**
     * Legge dalla console una stringa e stampa i dependenti che contengono nel
     * loro nome e cognome la stringa letta.
     */
    private void cerca() {
        Scanner input = new Scanner(System.in);
        String nc = inputString(input, "Nome e cognome: ", CHECK_NOMECOGNOME);
        for (Dipendente d : dipendenti) {
            if (d.getNomeCognome().contains(nc)) System.out.println(d);
        }
    }

    /**
     * Legge dalla console un codice e rimuove dall'archivio il dipendente con
     * quel codice, se esiste.
     */
    private void rimuovi() {
        Scanner input = new Scanner(System.in);
        long codice = Long.parseLong(inputString(input, "codice: ", CHECK_CODICE));
        int i = 0;
        for (Dipendente d : dipendenti) {
            if (d.getCodice() == codice) {
                System.out.println("Rimosso dipendente " + d.getNomeCognome());
                dipendenti[i] = null;
                for (int j = i; j < dipendenti.length - 1; j++) {
                    dipendenti[j] = dipendenti[j + 1];
                }
                dipendenti = Arrays.copyOf(dipendenti, dipendenti.length - 1);
                return;
            }
            i++;
        }
        System.out.println("Non è stato trovato nessun dipendente con codice " + codice +
                ", nessuna rimozione effettuata");
    }

    /**
     * Stampa sulla console i dati di tutti i dipendenti nell'archivio
     */
    private void tutti() {
        for (Dipendente d : dipendenti) {
            System.out.println(d.getNomeCognome() + " (cod. " + d.getCodice() + " stipendio: " + d.getStipendio() + ")");
            Dipendente.Contatti c = d.getContatti();
            System.out.println("  Indirizzo: " + c.getIndirizzo() + " Tel: " + c.getTelefono());
        }
    }

    private Dipendente[] dipendenti;    // Mantiene l'archivio dei dipendenti

    private static class CheckNomeCognome implements Checker {
        @Override
        public String valid(String s) {
            String r = checkString(s, " '", true);
            return (r != null ? r : (s.isEmpty() ? "Non può essere vuoto" : null));
        }
    }

    private static class CheckIndirizzo implements Checker {
        @Override
        public String valid(String s) {
            return checkString(s, " ',.0123456789", true);
        }
    }

    private static class CheckTelefono implements Checker {
        @Override
        public String valid(String s) {
            return checkString(s, " 0123456789", false);
        }
    }

    private static class CheckCodice implements Checker {
        @Override
        public String valid(String s) {
            return checkString(s, "0123456789", false);
        }
    }

    private static class CheckStipendio implements Checker {
        @Override
        public String valid(String s) {
            return checkString(s, "0123456789.", false);
        }
    }

    /**
     * Controlla che la stringa data non sia {@code null} e che i suoi caratteri
     * siano in {@code chars} o, se {@code letters} è {@code true}, che siano
     * lettere.
     *
     * @param s       la stringa da controllare
     * @param chars   caratteri permessi
     * @param letters se {@code true}, sono permesse anche le lettere
     * @return null se la stringa è valida, altrimenti una stringa con la
     * spiegazione dell'errore
     */
    private static String checkString(String s, String chars, boolean letters) {
        if (s == null) return "Non può essere null";
        for (char c : s.toCharArray())
            if (chars.indexOf(c) < 0 && (!letters || !Character.isLetter(c)))
                return "Il carattere '" + c + "' non è valido";
        return null;
    }
}