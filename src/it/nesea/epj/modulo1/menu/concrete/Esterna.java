package it.nesea.epj.modulo1.menu.concrete;

import it.nesea.epj.modulo1.menu.abstractmenu.MenuApp;

/**
 * @author Fabio Napoleoni
 */
public class Esterna {
    public static void main(String[] args) {  // Mette alla prova l'applicazione
        MathApp appCheStaInStessoPackage = new MathApp();
        MenuApp appCheStaInPackageDifferente = appCheStaInStessoPackage;
        // per altro esperimento
        MenuApp nuova = new MathApp();

        // notare che le due variabili puntano esattamente allo stesso oggetto
        System.out.println("Sono uguali? " + (appCheStaInStessoPackage == appCheStaInPackageDifferente));

        // la differenza tra le due variabili e' il package dove risiede il tipo
        // della variabile

        // con visibilita' protected !!
        // non lo posso fare perche' non estendo MenuApp
        // appCheStaInPackageDifferente.metodoProtetto();
        // questo invece lo posso fare perche' il riferimento punta allo stesso
        // oggetto ma ha un tipo diverso, il meccanismo di visibilita' e' basato
        // sullo static typing quindi la regola viene applicata in base al tipo
        appCheStaInStessoPackage.metodoProtetto();

        // non lo posso fare perche' e' in un altro package
        // appCheStaInPackageDifferente.metodoPackageProtected();
        // non lo posso fare perche' non e' ereditato da MenuApp
        // appCheStaInStessoPackage.metodoPackageProtected();

        // lo posso fare per via della tipizzazione statica
        // ma a runtime mi chiama l'implementazione di MathApp
        nuova.metodoEreditatoPublic();
    }

}