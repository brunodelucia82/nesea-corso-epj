package it.nesea.epj.modulo1.menu.concrete;

/**
 * @author Fabio Napoleoni
 */

import it.nesea.epj.modulo1.menu.abstractmenu.MenuApp;

import java.util.Scanner;


/**
 * Una semplice applicazione con menu testuale per il calcolo di funzioni
 * matematiche.
 */
public class MathApp extends MenuApp {

    String[] shortNames;
    String[] dataType;

    public MathApp() {
        super("Logaritmo naturale", "Radice quadrata", "Seno", "Coseno");
        shortNames = new String[] {"ln", "sqrt", "sin", "cos"};
        dataType = new String[] {"valore numerico", "valore numerico", "ampiezza angolo in radianti", "ampiezza angolo in radianti"};
    }

    /**
     * Esegue il calcolo relativo alla voce di menu scelta.
     *
     * @param choice il numero della voce di menu scelta
     */
    @Override
    protected void doMenu(int choice) {
        Scanner input = new Scanner(System.in);
        System.out.print("Digita " + dataType[choice - 1] + ": ");
        while (!input.hasNextDouble()) {
            input.next();    // Scarta qualsiasi input che non è un numero
        }
        double x = input.nextDouble();
        switch (choice) {
            case 1:
                System.out.println(shortNames[choice - 1] + "(" + x + ") = " + Math.log(x));
                break;
            case 2:
                System.out.println(shortNames[choice - 1] + "(" + x + ") = " + Math.sqrt(x));
                break;
            case 3:
                System.out.println(shortNames[choice - 1] + "(" + x + ") = " + Math.sin(x));
                break;
            case 4:
                System.out.println(shortNames[choice - 1] + "(" + x + ") = " + Math.cos(x));
                break;
        }
    }

    @Override
    protected void metodoProtetto() {
        // neanche questo fa nulla
        // posso chiamare il metodo della superclasse
        super.metodoProtetto();
        // questo non lo posso fare perche' sono in un package differente
        // super.metodoPackageProtected();
    }

    public static void main(String[] args) {  // Mette alla prova l'applicazione
        MathApp app = new MathApp();
        // MenuApp app = new MathApp(); // sarebbe la stessa identica cosa
        app.run(true);
        // visibilita dei metodi
        // lo posso chiamare perche' nonostante io sia in un package
        // differente sono in una sottoclasse, nelle sottoclassi
        // posso sempre "vedere" i metodi protetti
        app.metodoProtetto();
        // non lo posso chiamare perche' sono in un package differente
        // app.metodoPackageProtected();
    }

    // in questo metodo non posso cambiare la visibilita'
    // verso il basso (es. protected) perche' altrimenti
    // genererei un conflitto concettuale
    @Override
    public void metodoEreditatoPublic() {
    }

    // in questo caso si, ovvero nell'override
    // posso retringere i vincoli di visibilita'
    @Override
    public void metodoEreditatoProtected() {
    }
}
