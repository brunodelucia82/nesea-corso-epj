package it.nesea.epj.modulo1;

public class Dirigente extends Dipendente {
    private double bonus;

    public Dirigente(String nomeCognome, double stipendio){
        super(nomeCognome, stipendio);
    }

    public Dirigente(String nomeCognome, double stipendio, double bonus){
        this(nomeCognome, stipendio);
        setBonus(bonus);
    }
    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return "Dirigente{" +
                "nomeCognome='" + getNomeCognome() + '\'' +
                ", codice=" + getCodice() +
                ", stipendio=" + getStipendio() +
                ", bonus=" + bonus +
                '}';
    }
}
